/*
 * Archivo: NReinas.java 
 * Descripcion: Clase que representa una solución por backtracking del problema
 * de las n reinas.
 * Autor: Paul Baptista (10-10056) y Fabio Castro (10-10132). Grupo 09
 * Fecha:
 */

public class NReinas{

    private int ancho;
    private int alto;
    private int exitos = 0;
    private int fallos = 0;
    private int profundidad = 0;
    private List<int[]> soluciones = new MyList<int[]>();


    /*
     * Método que hace la resolución del problema, hace uso del método
     * resolverR, que es el que realmente hace la implementación del 
     * Backtracking recursivo.
     */
    private void resolver(){
      int[] tupla = new int[ancho];
      resolverR(tupla,0,0);
    }


    /*
     * Método que implementa el Backtracking y setea la información de nodos
     * fallidos y profundidad del "árbol" recorrido
     */
    private void resolverR(int[] tupla, int columna, int miprofundidad){

      int[] tuplatmp;
      boolean verif;

      miprofundidad++;

      if (miprofundidad>profundidad) {
        profundidad=miprofundidad;
      }

      //Itero la posición de las columnas
      for (int i=1; i<=alto ; i++) {

        tupla[columna]=i;

        //Verifico que el nodo actual sea válido para continuar con el recorrido
        verif = true;
        for (int j=columna-1; (j>=0)&&verif ; j--) {
          verif = verif && !( ( tupla[columna] == tupla[j] )
                              ||( tupla[columna] == tupla[j]+(columna-j) )
                              ||( tupla[columna] == tupla[j]-(columna-j) )
                            );
        }//Fin ciclo de Verificación
        
        if (verif){
          if (columna<ancho-1){
            resolverR(tupla,columna+1,miprofundidad);
          }
          else{
            tuplatmp = new int[tupla.length];
            System.arraycopy(tupla,0,tuplatmp,0,tupla.length);
            soluciones.add(tuplatmp);
            exitos++;
          }
        }
        else{
          fallos++;
        }
      }//Cierro ciclo de opciones
    }//Fin ResolverR

    /*
     * Crea el objeto que representa la solución del problema para un tablero de
     * tamaño nxn
     */
    public NReinas(int n){
      ancho = n;
      alto = n;
      resolver();
    }

    /*
     * Crea el objeto que representa la solución del problema para un tablero de
     * tamaño nxm
     */
    public NReinas(int n, int m){
      alto = n;
      ancho = m;
      resolver();
    }

    /*
     * Devuelve una lista con las soluciones del problema representado
     */
    public List<int[]> getSoluciones(){
      return soluciones;
    }

    /*
     * Devuelve la cantidad de soluciones que tiene el problema representado
     */
    public int getExitos(){
      return exitos;
    }

    /*
     * Devuelve las cantidad de nodos, en los que falla la verificación
     * correspondiente, hallados al hacer el recorrido del "arbol de búsqueda"
     * inherente al backtracking.
     */
    public int getFallos(){
      return fallos;
    }

    /*
     * Devuelve la profundidad del arbol de desiciones
     */
    public int getProfundidad(){
      return profundidad;
    }

 }