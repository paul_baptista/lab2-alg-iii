/*
 * Archivo: Main.java 
 * Descripcion: Implementa los requerimientos del Enunciado haciendo uso de
 * las clases y métodos de las demás acrchivos (Archivo.java, MyList.java,
 * NReinas.java).
 * Autor: Paul Baptista (10-10056) y Fabio Castro (10-10132). Grupo 09
 * Fecha:
 */
import java.io.*; 

public class Main {


   /*
    * Funcion println, imprime en pantalla la representación en String de los
    *   objetos que recibe como parametros
    */
   public static void print(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
   }

   /*
    * Funcion println, imprime en pantalla la representación en String de los
    *   objetos que recibe como parametros
    */
   public static void println(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
      System.out.println();
   }

   /*
    * Funcion ClearScreen imprime 25 saltos de linea y limpiando el terminal
    */

   public static void ClearScreen() {
      int i;
      for(i=0;i<=25;i++){
         println();
      }

   }
   /*
    * Funcion printlist, imprime en pantalla la representación en String de los
    *   objetos que contiene la lista pasada como parametro
    */
   public static void printlist(List lista) {
      ListIterator iter = lista.iterator();
      while (iter.hasNext()) {
         println(iter.next());      

      }
   }

    /*
    * Funcion printlist, imprime en pantalla la representación en String de los
    *   objetos que contiene la lista pasada como parametro
    */
   public static void printArray(int[] arr) {
    for (int i=0; i<arr.length-1; i++ ) {
      print(arr[i]+",");
    }
    print(arr[arr.length-1]);
   }


   /*
    * Main Principal que ejecuta el programa, llama a los métodos que hacen la
    * lectura del archivo, a los hacen la resolución del problema y a los que
    * escriben los resultados obtenidos.
    */
   public static void main(String args[]) throws Exception{
    
    int n;

    Archivo manejoarchivos = new Archivo();

    String entrada = args[0];
    String salida = args[1];

    int[] arr;

    manejoarchivos.openFile(entrada,salida);

    do{

      n = manejoarchivos.readFile();

      if (n>3){

        NReinas tablero = new NReinas(n);
        
        ListIterator<int[]> iter = tablero.getSoluciones().iterator();

        while (iter.hasNext()) {
          
          arr = iter.next();

          for (int i=0; i<arr.length-1; i++ ) {
            manejoarchivos.writeFile(arr[i]+",");
          }
          manejoarchivos.writeFile(arr[arr.length-1]+"\n");
        }

        manejoarchivos.writeFile("Éxitos: "+tablero.getExitos()+", Fallos: "
                                 +tablero.getFallos()
                                 +"\nProfundidad: "+tablero.getProfundidad()
                                 +"\n\n");
      }

    }while(n>3);

    manejoarchivos.closeFile();

  }

}
